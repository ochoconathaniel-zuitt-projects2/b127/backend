
const User = require('../models/User');
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth');




// USER REGISTRATION

module.exports.userSignup = (reqBody) =>{
		
		return User.find( { email: reqBody.email } ).then(result => {
	

	let newUser = new User({
		
		firstName: reqBody.firstName,
		lastName:  reqBody.lastName,
		email: 	   reqBody.email,
		password:  bcrypt.hashSync(reqBody.password, 10),
		address:   reqBody.address
	
	})

		return newUser.save().then((user, error) => {
		
		if(error){
			return false;
		}else{
		
			return true;
		     }
	
	      })

   
        

    })

}



// check exist

module.exports.checkEmailExists = (reqBody) => {
	return User.find( { email: reqBody.email } ).then(result => {

		if(result.length > 0){
			return true;
		}else{
		
			return false;
		}
	})
}




// detail check


module.exports.getProfile = (data) => {
	return User.findById( data.userId ).then(result => {
		result.password = "";
		return result;
	})
}




// USER AUTHENTICATION

module.exports.authenticate = (reqBody) => {
	return User.findOne( {email: reqBody.email} ).then(result => {

		if(result == null){
			
			return false;
		
		} else
			{
			
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			
			if(isPasswordCorrect){
			
				return { accessToken: auth.createAccessToken(result.toObject()) }
			}else{
			
				return false;
			}
		}
	})
}





// SET USER TO ADMIN (ADMIN ONLY)

module.exports.setAdmin = (reqParams) => {

	let updatedCourse = {
		isAdmin: true
	};
	
	
	return User.findByIdAndUpdate(reqParams.userId, updatedCourse).then((course, error) => {
		if(error){
			return false;
		}else{
			return ('User has been set to admin');
		}
	})
}






// CREATE AN ORDER

module.exports.checkOut = async (data) => {
	
	
	let isUserUpdated = await User.findById(data.userId).then(user => {
		
		user.orders.push({ productId : data.productId})

		return user.save().then((user, error) => {
			if(error) {
				return false;
			}else{
				return true;
			}
		})
	})


	let isProductUpdated = await Product.findById(data.productId).then(product => {
	
		product.ordered.push({ userId: data.userId})



		return product.save().then((course, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})
	})






	if(isUserUpdated && isProductUpdated){
		return true;
	}else{
		
		return false;
	}

}






//  GET ORDERS

module.exports.getOrder = (reqBody,reqParams) => {
		
		return User.find({email: reqBody.email}).then(result => {

			if(result.length > 0) {

				return User.findById(reqParams.userId).then(result =>{

					return result;

				})
			}

			else {

				return false;
			}

		})
	}









// GET ALL USER'S ORDERS

module.exports.getAllOrders = () => {
	return User.find({"orders.status": "Order is being processed"}).then(result => {
		return result;

	})
}









// Get all users

module.exports.getUsers = () => {
	return User.find({}).then(result => {
		return result;
	})
}




