
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth');


// CREATE A PRODUCT

/*module.exports.createItem = (reqBody) =>{


	let newProduct = new Product({
		
		productName: reqBody.productName,
		
		productPrice: reqBody.productPrice,
		
		productDescription: reqBody.productDescription
	
	})


	return newProduct.save().then((product, error) => {
		
		if(error){
			return false;
		}else{
	
			return true
		}
	})
}
*/




// Get all products


module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}








// GET ALL ACTIVE PRODUCTS

module.exports.getAllActiveProducts = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
}

// GET A SPECIFIC PRODUCT

module.exports.getSpecific = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;

	})
}




// UPDATE A PRODUCT


module.exports.updateItem = (reqParams,reqBody) => {
	
	let updatedProduct = {
		productName: reqBody.productName,
		productPrice: reqBody.productPrice,
		productDescription: reqBody.productDescription
	};

return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product,error) => {

	if(error){

		return ('Update Unsuccesful');
	}

	else {

		return true;
	}
})

}





// ARCHIVE A PRODUCT

module.exports.archiveProduct = (reqParams) => {

	let updatedProduct = {
		isActive: false
	}
	
	
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error){
			return ('Item archiving unsuccesful');
		}else{
			return true;
		}
	})
}





// DELETE A PRODUCT

module.exports.deleteItem = (reqParams) => {
return Product.findByIdAndRemove(reqParams.productId).then((deleted,error) => {
	if(error){
		return ('Failed to delete item!');
	} 

	else {
		return true;
	}
})

}




// Get tshirts

module.exports.Tops = () => {
	return Product.find({productDescription: "Tops"}).then(result => {
		return result;
	})
}




// Get pants


module.exports.Pants = () => {
	return Product.find({productDescription: "Pants"}).then(result => {
		return result;
	})
}



// Get shoes

module.exports.Shoes = () => {
	return Product.find({productDescription: "Shoes"}).then(result => {
		return result;
	})
}




// GET ACCESORIES

module.exports.Accesories = () => {
	return Product.find({productDescription: "Accesories"}).then(result => {
		return result;
	})
}









// multer

module.exports.createFile = (reqBody,reqFile) =>{




	let newProduct = new Product({
		
		productName: reqBody.productName,
		
		productPrice: reqBody.productPrice,
		
		productDescription: reqBody.productDescription,

		ProfilePicture: reqFile.filename
	
	})


	return newProduct.save().then((product, error) => {
		
		if(error){
			return false;
		}else{
	
			return ('Item successfully created');
		}
	})
}















// activate


module.exports.activateProduct = (params) => {
	const updates = { isActive:true }

	return Product.findByIdAndUpdate(params.productId,updates).then((doc,err) => {
		return (err) ? false :  true
	})

}