
const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	
	firstName : {
		type: String,
		required: [true, 'First name is required']
	},
	
	lastName: {
		type: String,
		required: [true, 'Last name is required']
	},
	
	email: {
		type: String,
		required: [true, 'Email is required'],
		
	}, 
	
	password: {
		type: String,
		required: [true, 'Password is required'],
	},
	
	
	address: {
		type: String,
		required: [true, 'Address is required']

	},

	isAdmin: {
		type: Boolean,
		default: false
	},
	
	orders: [
		{
			productId: {
				type: String,
				required: [true, 'product ID is required']
			},

		
			purchasedOn: {
				type: Date,
				default: new Date()
			},
			
			status: {
				type: String,
				default: "Order is being processed"
			}
		}
	]

})

module.exports = mongoose.model('User', userSchema);
